mod error;

use error::Error;

use rand::{
    distributions::{Distribution, Uniform},
    seq::SliceRandom,
    Rng,
};
use std::collections::{HashMap, HashSet};

#[cfg(feature = "rayon")]
use rayon::iter::{IndexedParallelIterator, IntoParallelRefMutIterator, ParallelIterator};

#[cfg(feature = "vis")]
use {
    colors_transform::{Color, Hsl},
    network_vis::{network::Network, node_options::NodeOptions},
};

pub type AdjacencyMatrix = Vec<Vec<bool>>;

#[derive(Debug, Clone)]
#[cfg_attr(test, derive(PartialEq, Eq))]
pub struct Graph {
    adjacency_matrix: AdjacencyMatrix,
}

impl Graph {
    pub fn from_adjacency_matrix(adjacency_matrix: AdjacencyMatrix) -> Result<Self, Error> {
        let order = adjacency_matrix.len();

        // ensure adjacency matrix is square
        if let Some(row) = adjacency_matrix
            .iter()
            .enumerate()
            .find(|(_, row)| row.len() != order)
        {
            return Err(Error::NotSquare(order, row.0, row.1.len()));
        }

        // ensure there are no self-loops by checking diagonal
        if let Some(vertex) = (0..order).find(|&vertex| adjacency_matrix[vertex][vertex]) {
            return Err(Error::SelfLoop(vertex));
        }

        // ensure adjacency matrix is symmetrical
        for u in 0..order {
            for v in (u + 1)..order {
                if adjacency_matrix[u][v] != adjacency_matrix[v][u] {
                    return Err(Error::NotSymmetrical(u, v));
                }
            }
        }

        Ok(Self { adjacency_matrix })
    }

    /// # Safety
    /// Ensure that the adjacency matrix is square, symmetrical and contains no true values on its diagonal
    #[must_use]
    pub unsafe fn from_adjacency_matrix_unchecked(adjacency_matrix: AdjacencyMatrix) -> Self {
        Self { adjacency_matrix }
    }

    // constructs a graph with no edges
    #[must_use]
    pub fn null_graph(order: usize) -> Self {
        Self {
            adjacency_matrix: vec![vec![false; order]; order],
        }
    }

    // constructs a graph where every distinct pair of vertices is connected by an edge
    #[must_use]
    pub fn complete_graph(order: usize) -> Self {
        let mut adjacency_matrix = vec![vec![true; order]; order];
        // remove self-loops
        (0..order).for_each(|i| {
            adjacency_matrix[i][i] = false;
        });

        Self { adjacency_matrix }
    }

    // constructs a random graph after Gilbert's model G(n, p)
    // every edge between distinct vertices independently exists with probability p
    #[must_use]
    pub fn random_graph<F>(order: usize, probability: F) -> Self
    where
        F: Into<f64>,
    {
        let probability: f64 = probability.into();
        let probability = probability.clamp(0.0, 1.0);

        let mut rng = rand::thread_rng();
        let mut random_graph = Self::null_graph(order);

        for u in 0..order {
            for v in (u + 1)..order {
                if rng.gen_bool(probability) {
                    // SAFETY: each pair of vertices is only visited once.
                    unsafe { random_graph.insert_edge_unchecked(u, v) };
                }
            }
        }

        random_graph
    }

    // constructs a random graph in the spirit of Gilbert's model G(n, p)
    // the additional parameter specifies an upper limit for the neighborhood diversity
    // first, a generator graph is constructed by generating a random graph with
    // #neighborhood_diversity_limit many vertices and the given edge probability
    // afterwards, for every vertex in the generator graph, a clique or an independent set
    // (based on the edge probability) is inserted into the resulting graph
    // finally, the sets of vertices are connected by edges analogous to the generator graph
    #[must_use]
    pub fn random_graph_nd_limited<F>(
        order: usize,
        probability: F,
        neighborhood_diversity_limit: usize,
    ) -> Self
    where
        F: Into<f64>,
    {
        let probability: f64 = probability.into();
        let probability = probability.clamp(0.0, 1.0);

        let mut rng = rand::thread_rng();
        let generator_graph = Self::random_graph(neighborhood_diversity_limit, probability);
        let mut random_graph = Self::null_graph(order);

        // randomly divides vertices into #neighborhood_diversity_limit many chunks
        // collects these dividers into sorted array as starting positions for the sets
        let set_start: Vec<usize> = {
            // vertex index 0 is reserved for the initial starting position
            let vertex_range = Uniform::from(1..=order);
            let mut set_dividers: HashSet<usize> =
                HashSet::with_capacity(neighborhood_diversity_limit);

            // avoids excessive iterations by generating at most order / 2 dividers
            if neighborhood_diversity_limit <= order / 2 {
                // insert into empty HashSet
                set_dividers.insert(0);
                while set_dividers.len() < neighborhood_diversity_limit {
                    set_dividers.insert(vertex_range.sample(&mut rng));
                }
            } else {
                // remove from 'full' HashSet
                set_dividers = (0..order).collect();
                while set_dividers.len() > neighborhood_diversity_limit {
                    set_dividers.remove(&vertex_range.sample(&mut rng));
                }
            }

            let mut set_start = Vec::from_iter(set_dividers);
            set_start.sort_unstable();
            set_start
        };

        for u_gen in 0..generator_graph.order() {
            let set_end_u = if u_gen == generator_graph.order() - 1 {
                order
            } else {
                set_start[u_gen + 1]
            };

            // decides wether the neighborhood is a clique or an independent set
            // if neighborhood is a clique, inserts all edges between distinct vertices
            if rng.gen_bool(probability) {
                for u in set_start[u_gen]..set_end_u {
                    for v in (u + 1)..set_end_u {
                        // SAFETY: each pair of vertices is only visited once.
                        unsafe { random_graph.insert_edge_unchecked(u, v) };
                    }
                }
            }

            // inserts edges between vertex sets based on edges in the generator_graph
            for &v_gen in generator_graph
                .neighbors(u_gen)
                .iter()
                .filter(|&&neighbor| neighbor > u_gen)
            {
                let set_end_v = if v_gen == generator_graph.order() - 1 {
                    order
                } else {
                    set_start[v_gen + 1]
                };
                for u in set_start[u_gen]..set_end_u {
                    for v in set_start[v_gen]..set_end_v {
                        // SAFETY: each pair of vertices is only visited once.
                        unsafe { random_graph.insert_edge_unchecked(u, v) };
                    }
                }
            }
        }

        random_graph
    }

    // shuffles vertex ids while retaining the original graph structure
    pub fn shuffle(&mut self) -> &mut Self {
        let order = self.order();
        let mut rng = rand::thread_rng();
        let mut vertices: Vec<usize> = (0..order).collect();
        vertices.shuffle(&mut rng);

        let mapping: HashMap<usize, usize> = vertices.into_iter().enumerate().collect();

        let mut shuffled_adjacency_matrix = vec![vec![false; order]; order];

        #[cfg(feature = "rayon")]
        shuffled_adjacency_matrix
            .par_iter_mut()
            .enumerate()
            .for_each(|(u, neighborhood)| {
                neighborhood
                    .par_iter_mut()
                    .enumerate()
                    .for_each(|(v, is_neighbor)| {
                        *is_neighbor = self.adjacency_matrix[mapping[&u]][mapping[&v]];
                    });
            });

        #[cfg(not(feature = "rayon"))]
        shuffled_adjacency_matrix
            .iter_mut()
            .enumerate()
            .for_each(|(u, neighborhood)| {
                neighborhood
                    .iter_mut()
                    .enumerate()
                    .for_each(|(v, is_neighbor)| {
                        *is_neighbor = self.adjacency_matrix[mapping[&u]][mapping[&v]];
                    });
            });

        self.adjacency_matrix = shuffled_adjacency_matrix;

        self
    }

    // inserts edge (u, v) into the graph
    // returns wether the edge was newly inserted:
    // graph did not contain edge: returns true
    // graph already contained edge: returns false
    pub fn insert_edge(&mut self, u: usize, v: usize) -> Result<bool, Error> {
        // returns error if index is out of bounds
        let order = self.order();
        for vertex in [u, v] {
            if vertex >= order {
                return Err(Error::OutOfBounds(order, vertex));
            }
        }
        if u == v {
            return Err(Error::SelfLoop(u));
        }

        // undirected graph -> symmetrical adjacency matrix
        // thus we only need to check for one direction but change both
        let newly_inserted = !self.adjacency_matrix[u][v];
        self.adjacency_matrix[u][v] = true;
        self.adjacency_matrix[v][u] = true;
        Ok(newly_inserted)
    }

    /// # Safety
    /// Ensure that the indices are in bounds and that the edge is not yet present.
    // inserts edge (u, v) into the graph without doing any sanity-checks
    pub unsafe fn insert_edge_unchecked(&mut self, u: usize, v: usize) {
        self.adjacency_matrix[u][v] = true;
        self.adjacency_matrix[v][u] = true;
    }

    // removes edge (u, v) from the graph
    // returns wether the edge was present:
    // graph did contain the edge: returns true
    // graph did not contain edge: returns false
    pub fn remove_edge(&mut self, u: usize, v: usize) -> Result<bool, Error> {
        // returns error if index is out of bounds
        let order = self.order();
        for vertex in [u, v] {
            if vertex >= order {
                return Err(Error::OutOfBounds(order, vertex));
            }
        }

        // undirected graph -> symmetrical adjacency matrix
        // thus we only need to check for one direction but change both
        let was_present = self.adjacency_matrix[u][v];
        self.adjacency_matrix[u][v] = false;
        self.adjacency_matrix[v][u] = false;
        Ok(was_present)
    }

    // returns number of vertices in the graph
    #[must_use]
    pub fn order(&self) -> usize {
        self.adjacency_matrix.len()
    }

    // returns neighbors of given vertex
    // a vertex is not it's own neighbor except for self-loops
    #[must_use]
    pub fn neighbors(&self, vertex: usize) -> Vec<usize> {
        (0..self.order())
            .filter(|&neighbor| self.adjacency_matrix[vertex][neighbor])
            .collect()
    }

    // returns neighbors of given vertex as a bool vector
    #[must_use]
    pub fn neighbors_as_bool_vector(&self, vertex: usize) -> &Vec<bool> {
        &self.adjacency_matrix[vertex]
    }

    // returns true if there is an edge between u and v
    #[must_use]
    pub fn is_edge(&self, u: usize, v: usize) -> bool {
        self.adjacency_matrix[u][v]
    }

    // returns degree of given vertex
    // a vertex is not it's own neighbor
    #[must_use]
    pub fn degree(&self, vertex: usize) -> usize {
        self.neighbors_as_bool_vector(vertex)
            .iter()
            .fold(0, |acc, &is_neighbor| acc + usize::from(is_neighbor))
    }

    // returns graph's density (number of edges / possible edges)
    #[must_use]
    #[allow(clippy::cast_precision_loss)]
    pub fn density(&self) -> f32 {
        let order = self.order();

        self.adjacency_matrix.iter().fold(0, |acc, row| {
            acc + row
                .iter()
                .fold(0, |acc, &is_neighbor| acc + usize::from(is_neighbor))
        }) as f32
            // possible edges := order^2 (counted every edge twice) - order (no self-loops)
            / (order as f32).mul_add(order as f32, -(order as f32))
    }

    // saves a text representation that can be parsed back into a graph
    // first line contains number of vertices
    // following lines list one edge each
    // vertex indices are separated by a comma: u,v
    // adds comments starting with '#', except when 'raw_output' == true
    pub fn export<P>(&self, path: P, raw_output: bool) -> std::io::Result<()>
    where
        P: AsRef<std::path::Path>,
    {
        let order = self.order();

        let mut output = if raw_output {
            format!("{}\n", order)
        } else {
            format!("# Number of Vertices\n{}\n\n# Edges\n", order)
        };
        for u in 0..order {
            for v in u..order {
                if self.adjacency_matrix[u][v] {
                    output.push_str(&format!("{},{}\n", u, v));
                }
            }
        }

        std::fs::write(path, output)
    }

    // saves an html document visualizing the graph
    // optional: vertices can be colored by group if a coloring partition is provided
    #[cfg(feature = "vis")]
    #[allow(clippy::cast_precision_loss)]
    pub fn visualize<P>(&self, path: P, coloring: Option<&[&[usize]]>) -> std::io::Result<()>
    where
        P: AsRef<std::path::Path>,
    {
        const SATURATION: f32 = 80.0;
        const LUMINANCE: f32 = 80.0;
        const DEFAULT_HUE: f32 = 180.0; // Teal
        const VERTEX_SHAPE: &str = "circle";

        let order = self.order();

        let default_color = Hsl::from(DEFAULT_HUE, SATURATION, LUMINANCE)
            .to_rgb()
            .to_css_hex_string();
        let na_color = Hsl::from(0.0, 0.0, 95.0).to_rgb().to_css_hex_string();
        let mut colors: Vec<String> = Vec::new();

        let mut vis_network = Network::new();

        if let Some(coloring) = coloring {
            // selects colors for vertex groups by splitting hue into equal parts
            // avoids borrow checker by generating vector of colors in it's own loop
            let group_count = coloring.len();
            for group_id in 0..group_count {
                let hue = 360.0 / group_count as f32 * group_id as f32;
                colors.push(
                    Hsl::from(hue, SATURATION, LUMINANCE)
                        .to_rgb()
                        .to_css_hex_string(),
                );
            }

            // inserts vertices by group with their corresponding color and group id
            let mut remaining_vertices = (0..order).collect::<HashSet<usize>>();
            for (group_id, &color_group) in coloring.iter().enumerate() {
                for vertex in color_group {
                    remaining_vertices.remove(vertex);
                    vis_network.add_node(
                        *vertex as _,
                        &format!("{}", group_id),
                        Some(vec![
                            NodeOptions::Hex(&colors[group_id]),
                            NodeOptions::Shape(VERTEX_SHAPE),
                        ]),
                    );
                }
            }

            // inserts remaining vertices (if not all vertices are included in the coloring)
            for vertex in remaining_vertices {
                vis_network.add_node(
                    vertex as _,
                    "N/A",
                    Some(vec![
                        NodeOptions::Hex(&na_color),
                        NodeOptions::Shape(VERTEX_SHAPE),
                    ]),
                );
            }
        } else {
            // no coloring provided, thus inserts all vertices with default color and no group id
            for vertex in 0..order {
                vis_network.add_node(
                    vertex as _,
                    "",
                    Some(vec![
                        NodeOptions::Hex(&default_color),
                        NodeOptions::Shape(VERTEX_SHAPE),
                    ]),
                );
            }
        }

        // inserts edges corresponding to those from the original graph
        for u in 0..order {
            for v in (u + 1)..order {
                if self.adjacency_matrix[u][v] {
                    vis_network.add_edge(u as _, v as _, None, false);
                }
            }
        }

        let path_as_str = path.as_ref().to_string_lossy();
        vis_network.create(&path_as_str)?;
        Ok(())
    }
}

// creates a graph from an input string
// first line contains number of vertices
// following lines list one edge each
// vertex indices are separated by a comma: u,v
// ignores: empty lines; lines starting with '#', '//' or '%'
impl std::str::FromStr for Graph {
    type Err = Error;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        // filter out comments and empty lines
        let mut relevant_lines = input.lines().map(str::trim).filter(|line| {
            !(line.is_empty()
                || line.starts_with('#')
                || line.starts_with("//")
                || line.starts_with('%'))
        });
        // first relevant line should contain the number of vertices
        let order = relevant_lines
            .next()
            .ok_or_else(|| Error::InvalidInput("no uncommented lines".to_string()))?
            .parse()
            .map_err(|_| Error::InvalidInput("error parsing order".to_string()))?;
        let mut graph = Self::null_graph(order);

        // for each remaining line, tries to split once at comma
        // then tries to parse both sides as vertex indices
        // finally, tries inserting new edge into the graph
        relevant_lines.try_for_each(|edge| -> Result<(), Self::Err> {
            let parse_error = format!("expected comma-separated vertex ids, received: '{}'", edge);

            let vertices = edge
                .split_once(',')
                .ok_or_else(|| Error::InvalidInput(parse_error.clone()))?;
            let u = vertices
                .0
                .parse()
                .map_err(|_| Error::InvalidInput(parse_error.clone()))?;
            let v = vertices
                .1
                .parse()
                .map_err(|_| Error::InvalidInput(parse_error.clone()))?;
            graph.insert_edge(u, v)?;
            Ok(())
        })?;

        Ok(graph)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::prelude::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn graph_from_str() {
        let input = "3
0,1
0,2
";

        let graph_parsed = input.parse::<Graph>().unwrap();

        let graph_truth = Graph {
            adjacency_matrix: vec![
                vec![false, true, true],
                vec![true, false, false],
                vec![true, false, false],
            ],
        };

        assert_eq!(graph_parsed, graph_truth);
    }

    #[test]
    fn graph_from_str_with_comments() {
        let input = "# VERTICES
3
# EDGES
0,1

0,2
// More comments
 //even with space in front
";

        let graph_parsed = input.parse::<Graph>().unwrap();

        let graph_truth = Graph {
            adjacency_matrix: vec![
                vec![false, true, true],
                vec![true, false, false],
                vec![true, false, false],
            ],
        };

        assert_eq!(graph_parsed, graph_truth);
    }

    #[test]
    fn random_graph_nd_limited() {
        let order = 100;
        let neighborhood_diversity_limit = 10;
        let probability = 0.5;
        let graph =
            Graph::random_graph_nd_limited(order, probability, neighborhood_diversity_limit);

        assert!(calc_nd_naive(&graph, Optimizations::none()).len() <= neighborhood_diversity_limit);
    }
}
