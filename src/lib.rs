// useful lints that are allowed by default
#![warn(
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unused_qualifications
)]
// enable more aggressive clippy lints
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::undocumented_unsafe_blocks)]
#![warn(clippy::use_debug)]
// disable lints that are too aggressive
#![allow(clippy::uninlined_format_args)] // inlined format args don't support F2 batch renaming (yet?)
#![allow(clippy::missing_panics_doc)]
#![allow(clippy::missing_errors_doc)]

pub mod graph;
pub mod prelude;

use graph::Graph;

use std::collections::BTreeMap;
use std::num::NonZeroUsize;
use std::sync::mpsc;
use std::thread;

pub type Partition = Vec<Vec<usize>>;

#[derive(Debug, Clone, Copy)]
pub struct Optimizations {
    pub degree_filter: bool,
    pub transitivity: bool,
}

impl Optimizations {
    #[must_use]
    pub const fn new(degree_filter: bool, transitivity: bool) -> Self {
        Self {
            degree_filter,
            transitivity,
        }
    }

    #[must_use]
    pub const fn none() -> Self {
        Self {
            degree_filter: false,
            transitivity: false,
        }
    }

    #[must_use]
    pub const fn all() -> Self {
        Self {
            degree_filter: true,
            transitivity: true,
        }
    }

    #[must_use]
    pub const fn degree_filter() -> Self {
        Self {
            degree_filter: true,
            transitivity: false,
        }
    }

    #[must_use]
    pub const fn transitivity() -> Self {
        Self {
            degree_filter: false,
            transitivity: true,
        }
    }
}

// returns true if u and v are of the same type
#[must_use]
fn same_type(graph: &Graph, u: usize, v: usize) -> bool {
    let u_neighbors = graph.neighbors_as_bool_vector(u);
    let v_neighbors = graph.neighbors_as_bool_vector(v);

    let small = u.min(v);
    let large = u.max(v);

    // compare neighborhoods excluding u and v
    // u_neighbors[u] (v_neighbors[v]) always false because no self-loops
    // u_neighbors[v] (v_neighbors[u]) always false because N(u)\{v} (N(v)\{u})
    let before_small_eq = u_neighbors[..small] == v_neighbors[..small];
    let in_between_eq = u_neighbors[small + 1..large] == v_neighbors[small + 1..large];
    let after_large_eq = u_neighbors[large + 1..] == v_neighbors[large + 1..];

    before_small_eq && in_between_eq && after_large_eq
}

#[must_use]
pub fn calc_nd_naive(graph: &Graph, optimizations: Optimizations) -> Partition {
    let order = graph.order();
    let mut partition: Partition = vec![];
    let mut classes = vec![None::<usize>; order];

    // collect degrees for all vertices
    let degrees: Vec<usize> = if optimizations.degree_filter {
        (0..order).map(|vertex| graph.degree(vertex)).collect()
    } else {
        vec![]
    };

    let mut nd: usize = 0;

    for u in 0..order {
        // only compare neighborhoods if v is not already in an equivalence class
        if optimizations.transitivity && classes[u].is_some() {
            continue;
        }

        if classes[u].is_none() {
            classes[u] = Some(nd);
            partition.push(vec![u]);
            nd += 1;
        }

        for v in (u + 1)..order {
            if optimizations.transitivity && classes[v].is_some()
                || optimizations.degree_filter && degrees[u] != degrees[v]
            {
                continue;
            }

            if same_type(graph, u, v) {
                if classes[v].is_none() {
                    partition[classes[u].unwrap()].push(v);
                }
                classes[v] = classes[u];
            }
        }
    }

    partition
}

#[must_use]
pub fn calc_nd_naive_concurrent(
    graph: &Graph,
    optimizations: Optimizations,
    thread_count: NonZeroUsize,
) -> Partition {
    let order = graph.order();

    // create asynchronous channel
    let (tx, rx) = mpsc::channel();

    // create scoped thread to allow borrowing the graph which is non 'static data
    thread::scope(|scope| {
        (0..thread_count.into()).for_each(|thread| {
            let thread_tx = tx.clone();
            let range = thread * order / thread_count..(thread + 1) * order / thread_count;

            // execute the regular algorithm on the calculated vertex range in a new thread
            scope.spawn(move || {
                let mut partition: Partition = vec![];
                let mut classified = vec![false; range.end - range.start];

                // collect degrees for all vertices
                let degrees: Vec<usize> = if optimizations.degree_filter {
                    range.clone().map(|vertex| graph.degree(vertex)).collect()
                } else {
                    vec![]
                };

                for u in range.clone() {
                    if classified[u - range.start] {
                        continue;
                    }

                    let mut neighborhood_class = vec![u];
                    for v in (u + 1)..range.end {
                        if optimizations.transitivity && classified[v - range.start]
                            || optimizations.degree_filter
                                && degrees[u - range.start] != degrees[v - range.start]
                        {
                            continue;
                        }

                        if same_type(graph, u, v) {
                            classified[v - range.start] = true;
                            neighborhood_class.push(v);
                        }
                    }
                    partition.push(neighborhood_class);
                }

                // pass resulting partition into the asynchronous channel
                thread_tx.send(partition)
            });
        });
    });

    // drop own transmitter to signal end of transmission
    drop(tx);

    merge_partitions(graph, rx.iter().collect())
}

#[must_use]
fn merge_partitions(graph: &Graph, mut partitions: Vec<Partition>) -> Partition {
    let mut destination_partition = vec![];
    while destination_partition.is_empty() {
        destination_partition = match partitions.pop() {
            Some(class) => class,
            None => return vec![],
        }
    }

    for source_partition in partitions {
        for mut source_class in source_partition {
            let mut found = false;
            for destination_class in &mut destination_partition {
                if same_type(graph, destination_class[0], source_class[0]) {
                    destination_class.append(&mut source_class);
                    found = true;
                    break;
                }
            }
            if !found {
                destination_partition.push(source_class);
            }
        }
    }

    destination_partition
}

#[must_use]
pub fn calc_nd_btree(graph: &Graph) -> Partition {
    let mut partition: Partition = Vec::new();
    let mut independent_sets: BTreeMap<&Vec<bool>, usize> = BTreeMap::new();
    let mut cliques: BTreeMap<Vec<bool>, usize> = BTreeMap::new();

    for vertex in 0..graph.order() {
        let independent_set_type = graph.neighbors_as_bool_vector(vertex);
        let mut clique_type; // will only be constructed if first search fails

        if let Some(&vertex_type) = independent_sets.get(independent_set_type) {
            // vertex type found in the 'independent set' BTree
            partition[vertex_type].push(vertex);
        } else if let Some(&vertex_type) = cliques.get({
            clique_type = independent_set_type.clone();
            clique_type[vertex] = true;
            &clique_type
        }) {
            // vertex type found in the 'clique' BTree
            partition[vertex_type].push(vertex);
        } else {
            // vertex type found in neither BTree
            // create new class and insert types into both BTrees
            let vertex_type = partition.len();
            partition.push(vec![vertex]);
            independent_sets.insert(independent_set_type, vertex_type);
            cliques.insert(clique_type, vertex_type);
        }
    }

    partition
}

#[must_use]
pub fn calc_nd_btree_concurrent(graph: &Graph, thread_count: NonZeroUsize) -> Partition {
    type VertexType = Vec<bool>;

    #[derive(Debug, Default, Clone)]
    struct Data<'is> {
        partition: Partition,
        independent_sets: BTreeMap<&'is Vec<bool>, usize>,
        cliques: BTreeMap<Vec<bool>, usize>,
    }

    // create asynchronous channel
    let (tx, rx) = mpsc::channel();

    // create scoped thread to allow borrowing the graph which is non 'static data
    thread::scope(|scope| {
        for thread in 0..thread_count.into() {
            let thread_tx = tx.clone();

            // execute the regular algorithm on the calculated vertex range in a new thread
            scope.spawn(move || {
                let mut partition: Partition = Vec::new();
                let mut independent_sets: BTreeMap<&Vec<bool>, usize> = BTreeMap::new();
                let mut cliques: BTreeMap<Vec<bool>, usize> = BTreeMap::new();

                let range = thread * graph.order() / thread_count
                    ..(thread + 1) * graph.order() / thread_count;

                for vertex in range {
                    let independent_set_type: &VertexType = graph.neighbors_as_bool_vector(vertex);
                    let mut clique_type; // will only be constructed if first search fails

                    if let Some(&vertex_type) = independent_sets.get(independent_set_type) {
                        // vertex type found in the 'independent set' BTree
                        partition[vertex_type].push(vertex);
                    } else if let Some(&vertex_type) = cliques.get({
                        clique_type = independent_set_type.clone();
                        clique_type[vertex] = true;
                        &clique_type
                    }) {
                        // vertex type found in the 'clique' BTree
                        partition[vertex_type].push(vertex);
                    } else {
                        // vertex type found in neither BTree
                        // create new class and insert types into both BTrees
                        let vertex_type = partition.len();
                        partition.push(vec![vertex]);
                        independent_sets.insert(independent_set_type, vertex_type);
                        cliques.insert(clique_type, vertex_type);
                    }
                }

                // pass results into the asynchronous channel
                thread_tx.send(Data {
                    partition,
                    independent_sets,
                    cliques,
                })
            });
        }
    });

    // drop own transmitter to signal end of transmission
    drop(tx);

    // collect results from threads
    let mut thread_data = rx.iter().collect::<Vec<_>>();

    // merge partitions into last element
    let mut collection = thread_data.pop().expect("len is non-zero");

    // merge neighborhood partitions
    for data in &mut thread_data {
        let mut not_found: Vec<(Option<&VertexType>, Option<&VertexType>)> =
            vec![(None, None); data.partition.len()];

        // check for equivalent neighborhood classes that are cliques
        for (clique_type, &class) in &data.cliques {
            if let Some(&vertex_type) = collection.cliques.get(clique_type) {
                collection.partition[vertex_type].append(&mut data.partition[class]);
            } else {
                not_found[class].0 = Some(clique_type);
            }
        }

        // check for equivalent neighborhood classes that are independent sets
        for (is_type, &class) in &data.independent_sets {
            if not_found[class].0.is_none() {
                continue;
            }
            if let Some(&vertex_type) = collection.independent_sets.get(is_type) {
                collection.partition[vertex_type].append(&mut data.partition[class]);
            } else {
                not_found[class].1 = Some(is_type);
            }
        }

        // insert remaining classes into collection
        for (class, (clique_type, is_type)) in not_found
            .iter()
            .enumerate()
            .filter(|(_class, found)| found.1.is_some() && found.0.is_some())
        {
            // insert clique type into collection
            collection
                .cliques
                .insert(clique_type.unwrap().clone(), class);
            // insert independent set type into collection
            collection.independent_sets.insert(is_type.unwrap(), class);
            // add vertices as new neighborhood class
            collection.partition.push(data.partition[class].clone());
        }
    }

    collection.partition
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::prelude::*;
    use pretty_assertions::assert_eq;
    use rand::Rng;
    use rayon::prelude::{IntoParallelIterator, IntoParallelRefMutIterator, ParallelIterator};

    const ORDER: usize = 100;
    const DENSITY: f32 = 0.5;
    const ND_LIMIT: usize = 20;
    const TEST_GRAPH_COUNT: usize = 100;
    const THREAD_COUNT: NonZeroUsize = {
        // SAFETY: 3 is non-zero.
        unsafe { NonZeroUsize::new_unchecked(3) }
    };

    // graph with a neighborhood diversity of 6
    const EXAMPLE_GRAPH: &str = "# Number of vertices
12

# Edges
0,1
0,2
1,2
2,9
2,10
2,11
2,3
3,9
3,10
3,11
3,4
3,5
4,6
4,7
4,8
5,6
5,7
5,8
6,7
6,8
7,8
9,10
9,11
10,11
";

    fn baseline(graph: &Graph) -> Partition {
        // closure replacing the same_type() function
        let same_type = |u: usize, v: usize| -> bool {
            let mut u_neighbors: Vec<bool> = graph.neighbors_as_bool_vector(u).clone();
            let mut v_neighbors: Vec<bool> = graph.neighbors_as_bool_vector(v).clone();

            // N(u) \ v
            u_neighbors[v] = false;
            // N(v) \ u
            v_neighbors[u] = false;

            // equal comparison works because neighbors are bool vector
            u_neighbors == v_neighbors
        };

        let order: usize = graph.order();
        let mut partition: Partition = Vec::new();
        let mut classes: Vec<Option<usize>> = vec![None; order];
        let mut nd: usize = 0;

        for u in 0..order {
            if classes[u].is_none() {
                classes[u] = Some(nd);
                partition.resize((nd + 1).max(partition.len()), vec![]);
                partition[nd].push(u);
                nd += 1;
            }
            for v in (u + 1)..order {
                if same_type(u, v) {
                    classes[v] = classes[u];
                    if !partition[classes[u].unwrap()].contains(&v) {
                        partition[classes[u].unwrap()].push(v);
                    }
                }
            }
        }

        partition
    }

    fn test_graphs() -> Vec<Graph> {
        (0..TEST_GRAPH_COUNT)
            .map(|_| Graph::random_graph_nd_limited(ORDER, DENSITY, ND_LIMIT))
            .collect::<Vec<Graph>>()
    }

    fn all_unique(partition: &Partition, order: usize) {
        let mut counter = std::collections::HashMap::new();

        for class in partition {
            for &vertex in class {
                *counter.entry(vertex).or_insert(0) += 1;
            }
        }

        if order == 0 {
            assert_eq!(counter.len(), order, "counter len != order");
            assert_eq!(counter.keys().min(), None, "counter min != None");
            assert_eq!(counter.keys().max(), None, "counter max != None");
        } else {
            assert_eq!(counter.len(), order, "counter len != order");
            assert_eq!(counter.keys().min(), Some(&0), "counter min != 0");
            assert_eq!(
                counter.keys().max(),
                Some(&(order - 1)),
                "counter max != order - 1"
            );
            assert!(counter.values().all(|&count| count == 1), "duplicate value");
        }
    }

    fn compare_all(graph: &Graph, expected: usize) {
        let order = graph.order();

        let partitions = &[
            baseline(graph),
            calc_nd_naive(graph, Optimizations::none()),
            calc_nd_naive(graph, Optimizations::degree_filter()),
            calc_nd_naive(graph, Optimizations::transitivity()),
            calc_nd_naive(graph, Optimizations::all()),
            calc_nd_naive_concurrent(graph, Optimizations::none(), THREAD_COUNT),
            calc_nd_naive_concurrent(graph, Optimizations::degree_filter(), THREAD_COUNT),
            calc_nd_naive_concurrent(graph, Optimizations::transitivity(), THREAD_COUNT),
            calc_nd_naive_concurrent(graph, Optimizations::all(), THREAD_COUNT),
            calc_nd_btree(graph),
            calc_nd_btree_concurrent(graph, THREAD_COUNT),
        ];

        for partition in partitions {
            // check for correct value of neighborhood diversity
            assert_eq!(partition.len(), expected);
            // check for uniqueness of vertices in partition
            all_unique(partition, order);
        }
    }

    #[test]
    fn all_algorithms_on_example() {
        let graph = EXAMPLE_GRAPH
            .parse::<Graph>()
            .unwrap_or_else(|error| panic!("error parsing input: {}", error));

        compare_all(&graph, 6);
    }

    #[test]
    fn all_algorithms_on_example_shuffled() {
        let mut graph = EXAMPLE_GRAPH
            .parse::<Graph>()
            .unwrap_or_else(|error| panic!("error parsing input: {}", error));

        graph.shuffle();

        compare_all(&graph, 6);
    }

    #[test]
    fn all_algorithms_on_test_graphs() {
        test_graphs().par_iter_mut().for_each(|graph| {
            let expected = baseline(graph).len();

            compare_all(graph, expected);
        });
    }

    #[test]
    fn all_algorithms_on_test_graphs_shuffled() {
        test_graphs().par_iter_mut().for_each(|graph| {
            let expected = baseline(graph).len();

            graph.shuffle();

            compare_all(graph, expected);
        });
    }

    #[test]
    fn fuzzing_gilbert() {
        (0..100).into_par_iter().for_each(|_| {
            let mut rng = rand::thread_rng();
            let order = rng.gen_range(0..=100);
            let probability = rng.gen::<f32>();

            let mut fuzzy_graph = Graph::random_graph(order, probability);

            let expected = baseline(&fuzzy_graph).len();

            fuzzy_graph.shuffle();

            compare_all(&fuzzy_graph, expected);
        });
    }

    #[test]
    fn fuzzing_nd_limit() {
        (0..100).into_par_iter().for_each(|_| {
            let mut rng = rand::thread_rng();
            let order = rng.gen_range(2..=100);
            let neighborhood_diversity_limit = rng.gen_range(0..=order);
            let probability: f32 = rng.gen();

            let mut fuzzy_graph =
                Graph::random_graph_nd_limited(order, probability, neighborhood_diversity_limit);

            let expected = baseline(&fuzzy_graph).len();

            fuzzy_graph.shuffle();

            compare_all(&fuzzy_graph, expected);
        });
    }

    #[test]
    fn empty_graph() {
        let null_graph = Graph::null_graph(0);
        let expected = 0;
        compare_all(&null_graph, expected);
    }

    #[test]
    fn null_graph() {
        let null_graph = Graph::null_graph(ORDER);
        let expected = 1;
        compare_all(&null_graph, expected);
    }

    #[test]
    fn complete_graph() {
        let complete_graph = Graph::complete_graph(ORDER);
        let expected = 1;
        compare_all(&complete_graph, expected);
    }
}
